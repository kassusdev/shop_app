import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import ProductContent from "../components/src/components/ProductContent.vue";
import ProductsList from "../components/src/components/ProductsList.vue";
import Carts from "../components/src/components/Carts.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
    {
    path: "/products",
    name: "ProductContent",
    component: ProductContent
  },
   {
    path: "/productslist",
    name: "ProductsList",
    component: ProductsList
  },
   {
    path: "/carts",
    name: "Carts",
    component: Carts
  },
 
];

const router = new VueRouter({
  routes
});

export default router;
